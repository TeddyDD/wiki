package models

import "time"

// NoteMetadata is stuct that contains all mandatory fields of note
// Note itself in raw form is just array of bytes
type NoteMetadata struct {
	Title   string                 `json:"title,omitempty"`
	Type    string                 `json:"type,omitempty"`
	User    string                 `json:"user,omitempty"`
	Tags    []string               `json:"tags,omitempty"`
	Created time.Time              `json:"created,omitempty"`
	Updated time.Time              `json:"updated,omitempty"`
	Fields  map[string]interface{} `json:"fields,omitempty"`
}

// Log impements Loggable for NoteMetadaa
func (n NoteMetadata) Log() map[string]interface{} {
	return map[string]interface{}{
		"title": n.Title,
		"type":  n.Type,
		"tags":  n.Tags,
	}
}

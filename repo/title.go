package repo

import (
	"regexp"
	"strings"
	// register extension for markdown mime type
)

var slashes = regexp.MustCompile(`^\/+|\/$`)
var spaceSlahes = regexp.MustCompile(`\s?(\/)\s?`)
var multipleSlashes = regexp.MustCompile(`\/{2,}`)
var pathTraversal = regexp.MustCompile(`\.{1,2}\/`)
var illegal = regexp.MustCompile(`[\\:*?\"'<>|~$]`)

// SanitizeTitle ensures that it will be possible to save
// note with given title as file.
// This is loosy process, before using result of this function, ensure it does not collide
// with exisiting key.
// Storage layer can impose additional restrictions but they must be loseless.
func SanitizeTitle(title string) string {
	title = strings.ToValidUTF8(title, "")
	title = pathTraversal.ReplaceAllString(title, "")
	title = spaceSlahes.ReplaceAllString(title, "$1")
	title = multipleSlashes.ReplaceAllString(title, "/")
	title = slashes.ReplaceAllString(title, "")
	title = illegal.ReplaceAllString(title, "_")
	title = strings.Join(strings.Fields(title), " ")
	return title
}

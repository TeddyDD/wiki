package repo

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNormalizeTitle(t *testing.T) {
	tests := []struct {
		title string
		want  string
	}{
		{
			title: "foo",
			want:  "foo",
		},
		{
			title: "Simple Note - chapter 1",
			want:  "Simple Note - chapter 1",
		},
		{
			title: " spaces around\t",
			want:  "spaces around",
		},
		{
			title: "tab\tinside",
			want:  "tab inside",
		},
		{
			title: "/root",
			want:  "root",
		},
		{
			title: "///foo/bar/",
			want:  "foo/bar",
		},
		{
			title: "/ foo bar / baz /",
			want:  "foo bar/baz",
		},
		{
			title: "foo////bar",
			want:  "foo/bar",
		},
		{
			title: "foo\nbar",
			want:  "foo bar",
		},
		{
			title: `\:*?\"'<>|~$`,
			want:  "____________",
		},
		{
			title: "a\xc5z",
			want:  "az",
		},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("sanitize title: %s", tt.title), func(t *testing.T) {
			require.Equal(t, tt.want, SanitizeTitle(tt.title))
		})
	}
}

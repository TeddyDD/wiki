// Package repo repreents repository of notes
package repo

import (
	"context"
	"errors"
	"io"

	"teddydd.me/pkg/wiki/models"
)

// Repo is genereic interface for notes repositories
type Repo interface {
	Get(context.Context, string) (*models.NoteMetadata, error)
	GetContent(context.Context, *models.NoteMetadata, io.Writer) error

	Has(context.Context, string) bool
	List(context.Context) ([]models.NoteMetadata, error)

	Create(context.Context, models.NoteMetadata) error

	Update(context.Context, models.NoteMetadata) error
	UpdateContent(context.Context, models.NoteMetadata, io.Reader) error

	Delete(context.Context, models.NoteMetadata) error
}

var (
	ErrExists    = errors.New("note already exists")
	ErrNotExists = errors.New("note not exists")
	ErrNoContent = errors.New("note does not contain any content")
)

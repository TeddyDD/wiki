package repo

import (
	"context"
	"errors"
	"io"

	"teddydd.me/pkg/wiki/log"
	"teddydd.me/pkg/wiki/models"
	"teddydd.me/pkg/wiki/repo/storage"
)

type localRepo struct {
	s storage.Storage
}

var _ Repo = &localRepo{}

var l = log.Global.Sub(log.ModuleRepo)

var (
	// ErrEmptyTitle indicates that user tired to use empty title
	ErrEmptyTitle = errors.New("title is empty")
)

func (r *localRepo) Get(ctx context.Context, title string) (*models.NoteMetadata, error) {
	l.Debug().Str("title", title).Msg("retrieving note metadata")

	if title == "" {
		l.Debug().Err(ErrEmptyTitle).Msg("empty title")
		return nil, ErrEmptyTitle
	}
	if !r.Has(ctx, title) {
		return nil, ErrNotExists
	}
	return nil, nil
}

func (r *localRepo) GetContent(_ context.Context, _ *models.NoteMetadata, _ io.Writer) error {
	panic("not implemented") // TODO: Implement
}

func (r *localRepo) Has(_ context.Context, title string) bool {
	return r.s.Has(title)
}

func (r *localRepo) List(_ context.Context) ([]models.NoteMetadata, error) {
	panic("not implemented") // TODO: Implement
}

func (r *localRepo) Create(_ context.Context, _ models.NoteMetadata) error {
	panic("not implemented") // TODO: Implement
}

func (r *localRepo) Update(_ context.Context, _ models.NoteMetadata) error {
	panic("not implemented") // TODO: Implement
}

func (r *localRepo) UpdateContent(_ context.Context, _ models.NoteMetadata, _ io.Reader) error {
	panic("not implemented") // TODO: Implement
}

func (r *localRepo) Delete(_ context.Context, _ models.NoteMetadata) error {
	panic("not implemented") // TODO: Implement
}

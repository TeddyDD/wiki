package storage

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"teddydd.me/pkg/wiki/mime"
	"teddydd.me/pkg/wiki/models"
)

var testExtensions = mime.InteralTypes()

func TestToKey(t *testing.T) {

	type testCase struct {
		name       string
		title      string
		mime       string
		expect     string
		expectMeta string
	}

	tests := []testCase{
		{
			name:       "already have an extension",
			title:      "foo.json",
			mime:       "application/json",
			expect:     "foo.json",
			expectMeta: "foo.json.meta",
		},
	}

	for m, e := range testExtensions {
		tests = append(tests, testCase{
			name:       fmt.Sprintf("note with mime %s", m),
			title:      "foo",
			mime:       m,
			expect:     "foo" + e,
			expectMeta: "foo" + e + metaSuffix,
		}, testCase{
			name:       fmt.Sprintf("note with type %s and ext in name", m),
			title:      "foo.myext",
			mime:       m,
			expect:     "foo.myext",
			expectMeta: fmt.Sprintf("%s.meta", "foo.myext"),
		})
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			note := models.NoteMetadata{
				Title: tc.title,
				Type:  tc.mime,
			}

			key := ToKey(note.Title, note.Type)
			metaKey := ToMetaKey(note.Title, note.Type)

			require.Equal(t, tc.expect, key)
			require.Equal(t, tc.expectMeta, metaKey)
			require.NotEqual(t, key, metaKey)
		})
	}
}

package storage

import (
	"regexp"

	"teddydd.me/pkg/wiki/mime"
)

var extensions = regexp.MustCompile(`(\.\w+$)`)

const (
	metaSuffix = ".meta"
)

// AddExtensionToTitle appends default extension to sanitized title
// based on mime type
func AddExtensionToTitle(title, mimeType string) string {
	if !extensions.MatchString(title) && mimeType != "" {
		ext, err := mime.ExtensionsByType(mimeType)
		if err == nil {
			if len(ext) >= 0 {
				title = title + ext[0]
			}
		}
	}
	return title
}

// ToKey returns kv key for storing given note
func ToKey(title, mimeType string) string {
	title = AddExtensionToTitle(title, mimeType)
	return convertTokey(title, false)
}

// ToMetaKey returns kv key for storing given note metadata
func ToMetaKey(title, mimeType string) string {
	title = AddExtensionToTitle(title, mimeType)
	return convertTokey(title, true)
}

func convertTokey(title string, meta bool) string {
	if meta {
		return title + metaSuffix
	}
	return title
}

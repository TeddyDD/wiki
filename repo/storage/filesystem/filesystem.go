// Package filesystem implements storage intereface using diskv as store for
// notes
package filesystem

import (
	"fmt"
	"io"

	"github.com/peterbourgon/diskv"
	"teddydd.me/pkg/wiki/log"
	"teddydd.me/pkg/wiki/repo/storage"
)

var l = log.Global.Sub(log.ModuleStorage)

const (
	keyLog   = "key"
	keyTitle = "title"
)

type fsStorage struct {
	DB *diskv.Diskv
}

var _ storage.Storage = &fsStorage{}

// Options is configuration for filesystem notes storage
type Options struct {
	// Path is root path where notes will be stored
	Path string
}

// New creates new filesystem based storage that uses
// diskv internally
func New(o Options) storage.Storage {
	return &fsStorage{
		DB: diskv.New(diskv.Options{
			BasePath: o.Path,
		}),
	}
}

func (f *fsStorage) Set(key string, r io.Reader) error {
	l.Debug().Str(keyLog, key).Msg("writing key to storage")

	err := f.DB.WriteStream(key, r, true)
	if err != nil {
		l.Debug().
			Err(err).
			Str(keyLog, key).
			Msg("failed to write")
		return err
	}
}

func (f *fsStorage) get(key string, w io.Writer) error {
	tmp, err := f.DB.ReadStream(key, false)
	if err != nil {
		return err
	}
	defer func() error {
		return tmp.Close()
	}()
	io.Copy(w, tmp)
}

func (f *fsStorage) SetMeta(string, io.Reader) error {
	return nil
}

func (f *fsStorage) Get(title string, w io.Writer) error {
	l.Debug().Str(keyTitle, title).Msg("retrieving title")

	key := storage.ToKey(title)

	if !f.DB.Has(key) {
		l.Debug().Str(keyTitle, key).Msg("key not found")
		return storage.ErrNotFound
	}

	rc, err := f.DB.ReadStream(key, false)
	if err != nil {
		l.Debug().Err(err).Str(keyLog, key).Msg("failed to read stream")
		return err
	}
	defer rc.Close()

	_, err = io.Copy(w, rc)
	if err != nil {
		l.Debug().Err(err).Str(keyLog, key).Msg("failed to copy stream")
		return fmt.Errorf("error copying key %s: %w", key, err)
	}
	return nil
}

func (f *fsStorage) GetMeta(key string, w io.Writer) error {
	return nil
}

func (f *fsStorage) Has(key string) bool {
	return f.DB.Has(key)

}

func (f *fsStorage) Del(_ string) error {
	panic("not implemented") // TODO: Implement
}

func (f *fsStorage) List() ([]string, error) {
	panic("not implemented") // TODO: Implement
}

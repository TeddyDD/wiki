package storage

import (
	"errors"
	"io"
)

// Storage is interface for implementing notes storage
// It operates on raw titles and buffers
type Storage interface {
	Set(string, io.Reader) error
	Get(string, io.Writer) error

	SetMeta(string, io.Reader) error
	GetMeta(string, io.Writer) error

	Del(string) error
	Has(string) bool

	List() ([]string, error)
}

var ErrNotFound = errors.New("key not found")

package index

// Indexable is usually Note
type Indexable interface {
	Title() string
	References() []string
	Tags() []string
}

// Index keeps index of links between note titles to speed up lookup
type Index interface {
	TaggedWith(string) []string
	ReferedFrom(string) []string

	Insert(Indexable)
}

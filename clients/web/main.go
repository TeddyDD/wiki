package main

import (
	"flag"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"teddydd.me/pkg/wiki/log"
	"teddydd.me/pkg/wiki/rest"
)

func main() {

	dev := flag.Bool("dev", false, "dev friendly logs")
	addr := flag.String("listen", ":33333", "listening addres")
	flag.Parse()

	// dev logs
	if *dev {
		log.Global = log.NewDefault(*dev, log.DebugLevel, os.Stdout)
	}
	l := log.Global.Sub(log.ModuleWebUI)

	router := chi.NewRouter()
	router.Mount("/api", rest.New())

	server := http.Server{
		Addr:              *addr,
		Handler:           router,
		ReadTimeout:       time.Second * 10,
		ReadHeaderTimeout: time.Second * 10,
		WriteTimeout:      time.Second * 10,
		IdleTimeout:       time.Second * 30,
	}

	l.Info().Addres(*addr).Msg("starting server")
	err := server.ListenAndServe()
	l.Info().Err(err).Msg("server exit")
}

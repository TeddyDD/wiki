// Package log is global wiki logger
package log

type Logger interface {
	Debug() LoggerData
	Info() LoggerData
	Sub(module) Logger
}

// LoggerData is generic interface used for logging in wiki
type LoggerData interface {
	Msg(string)

	Str(string, string) LoggerData
	Addres(string) LoggerData
	Caller() LoggerData
	Err(error) LoggerData
	Middleware(string) LoggerData
	Query(string, string) LoggerData

	Map(map[string]interface{}) LoggerData
	Custom(Loggable) LoggerData
}

type Loggable interface {
	Log() map[string]interface{}
}

var _ Logger = &defaultLogger{}

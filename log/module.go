package log

//go:generate stringer -trimprefix=Module -type=module
type module int

const (
	ModuleWebUI module = iota
	ModuleMiddleware
	ModuleStorage
	ModuleRepo
)

package log

import "github.com/rs/zerolog"

type loglevel int

const (
	InfoLevel loglevel = iota
	DebugLevel
)

func (l loglevel) toZerolog() zerolog.Level {
	switch l {
	case InfoLevel:
		return zerolog.InfoLevel
	case DebugLevel:
		return zerolog.DebugLevel

	default:
		panic("wrong log level")
	}
}

package log

import (
	"io"
	"os"

	"github.com/rs/zerolog"
)

type defaultLogger struct {
	Logger zerolog.Logger
}

// Global wiki logger
var Global Logger = NewDefault(false, InfoLevel, os.Stdout)

// NewDefault creates new default logger
func NewDefault(dev bool, level loglevel, output io.Writer) Logger {
	res := defaultLogger{
		Logger: zerolog.New(output).
			Level(level.toZerolog()).
			With().Timestamp().
			Logger(),
	}
	if dev {
		res.Logger = res.Logger.Output(zerolog.ConsoleWriter{Out: output})
	}
	return res
}

type loggerData struct {
	ev     *zerolog.Event
	caller bool
}

var _ LoggerData = &loggerData{}

func (l *loggerData) Addres(addr string) LoggerData {
	l.ev.Str(keyAddres, addr)
	return l
}

func (l *loggerData) Caller() LoggerData {
	l.caller = true
	return l
}

func (l *loggerData) Err(e error) LoggerData {
	l.ev.Err(e)
	return l
}

func (l *loggerData) Middleware(m string) LoggerData {
	l.ev.Str(keyMiddleware, m)
	return l
}

func (l *loggerData) Map(m map[string]interface{}) LoggerData {
	l.ev.Fields(m)
	return l
}

func (l *loggerData) Custom(obj Loggable) LoggerData {
	l.Map(obj.Log())
	return l
}

func (l *loggerData) Query(k, v string) LoggerData {
	l.ev.Dict(keyURLQueryParam, zerolog.Dict().Str(k, v))
	return l
}

func (l *loggerData) Str(k, v string) LoggerData {
	l.ev.Str(k, v)
	return l
}

func (l *loggerData) Msg(msg string) {
	if l.caller {
		l.ev.Caller(1)
	}
	l.ev.Msg(msg)
}

// Logger methods

func (d defaultLogger) Sub(m module) Logger {
	return defaultLogger{
		Logger: d.Logger.With().Str("module", m.String()).Logger(),
	}
}

func (d defaultLogger) Debug() LoggerData {
	return &loggerData{
		ev:     d.Logger.Debug(),
		caller: false,
	}
}

func (d defaultLogger) Info() LoggerData {
	return &loggerData{
		ev:     d.Logger.Info(),
		caller: false,
	}
}

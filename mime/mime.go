// Package mime registers few mime types that are not shipped with Go stdlib
// It should be imported for side effects
package mime

import (
	"mime"
	"sync"
)

var once sync.Once

var internalMimeTypes = map[string]string{
	"application/json": ".json",
	"application/pdf":  ".pdf",
	"application/wasm": ".wasm",
	"image/gif":        ".gif",
	"image/jpeg":       ".jpeg",
	"image/png":        ".png",
	"image/svg+xml":    ".svg",
	"image/webp":       ".webp",
	"text/css":         ".css",
	"text/html":        ".html",
	"text/javascript":  ".js",
	"text/markdown":    ".md",
	"text/xml":         ".xml",
}

func init() {
	once.Do(func() {
		for m, e := range internalMimeTypes {
			mime.AddExtensionType(e, m)
		}
	})
}

// InteralTypes returns map of internal mime types to extensions
func InteralTypes() map[string]string {
	return internalMimeTypes
}

// ExtensionsByType is wrapper for mime.ExtensionsByType
// that returns predefined extension for some mimetypes
func ExtensionsByType(mimeType string) ([]string, error) {
	if ext, ok := internalMimeTypes[mimeType]; ok {
		return []string{ext}, nil
	}
	return mime.ExtensionsByType(mimeType)
}

// TypeByExtension is thin wrapper for mime.TypeByExtension
func TypeByExtension(ext string) string {
	return mime.TypeByExtension(ext)
}

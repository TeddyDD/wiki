package rest

import (
	"net/http"

	"github.com/go-chi/chi"
	"teddydd.me/pkg/wiki/log"
)

var l = log.Global.Sub(log.ModuleMiddleware)

func noteCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		title := chi.URLParam(r, "title")
		l.Debug().Query("title", title).Msg("")

		next.ServeHTTP(w, r)
	})
}

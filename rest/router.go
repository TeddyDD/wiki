package rest

import (
	"github.com/go-chi/chi"
)

// New creeates new Wiki API router
func New() *chi.Mux {
	r := chi.NewRouter()
	setupRoutes(r)
	return r
}

func setupRoutes(r *chi.Mux) {
	r.Route("/v0", func(r chi.Router) {
		r.Route("/note", func(r chi.Router) {
			r.Route("/{title}", func(r chi.Router) {
				r.Use(noteCtx)
				r.Get("/", getNoteHandler)
			})
		})
	})
}

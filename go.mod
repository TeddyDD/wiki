module teddydd.me/pkg/wiki

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/google/btree v1.0.0 // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.6.1
)
